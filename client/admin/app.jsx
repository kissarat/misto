import React, {Component} from 'react'
import Menu from '../widget/menu.jsx';

const menu = Menu.normalize([
  ['Статьи', '/articles']
])

export default class Admin extends Component {
  render() {
    return <div id="admin" className="layout admin">
      <div className="container">
        <nav><Menu items={menu}/></nav>
        <div className="content">{this.props.children}</div>
      </div>
    </div>
  }
}
