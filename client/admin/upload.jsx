import React, {Component} from 'react'
import Form from '../widget/form.jsx'
import {FormGroup, ControlLabel, ButtonToolbar, Button} from 'react-bootstrap'
import {FieldGroup} from '../widget/field.jsx'
import 'whatwg-fetch'
import api from '../connect/api.jsx'
import {Link, browserHistory} from 'react-router'
import {pick} from 'lodash'

export default class Upload extends Form {
  componentWillReceiveProps(props) {
    this.setState({})
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  async submit() {
    if (this.state.file) {
      const body = new FormData();
      ['title', 'description'].forEach((name) => {
        if (this.state[name] && this.state[name].trim()) {
          body.append(name, this.state[name])
        }
      })
      body.append('album', 'Xbm5E')
      body.append('type', 'file')
      body.append('image', this.state.file)
      const r = await fetch('https://api.imgur.com/3/image', {
        method: 'POST',
        body,
        headers: {
          Authorization: 'Bearer 0146ba0c80d12dac75d82cde39cf8228f2da4f65'
        }
      })
      const {success, data} = await r.json()
      if (success) {
        const image = await api.send('image/create', data)
        this.setState({url: image.link})
      }
    }
  }

  changeFile = e => {
    this.setState({file: e.target.files[0]})
  }

  render() {
    const image = this.state.url ? <img src={this.state.url}/> : ''
    return <form className="page upload" onSubmit={this.onSubmit}>
      <h1>Загрузка картинки</h1>
      <FieldGroup
        type="text"
        label="Название"
        name="title"
        value={this.state.title || ''}
        onChange={this.onChange}
      />
      <FieldGroup
        type="textarea"
        label="Описание"
        name="description"
        value={this.state.description || ''}
        onChange={this.onChange}
      />
      <input type="file" accept="image/*" onChange={this.changeFile}/>
      <ButtonToolbar>
        <Button bsStyle="success" type="submit">Загрузить</Button>
      </ButtonToolbar>
      {image}
    </form>
  }
}
