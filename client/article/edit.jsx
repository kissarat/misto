import React, {Component} from 'react'
import Form from '../widget/form.jsx'
import {FormGroup, ControlLabel, ButtonToolbar, Button} from 'react-bootstrap'
import {FieldGroup} from '../widget/field.jsx'
import {translitURL} from './utils.jsx'
import api from '../connect/api.jsx'
import {Link, browserHistory} from 'react-router'
import Menu from '../widget/menu.jsx'

export class RichEdit extends Component {
  textarea = target => {
    if (target) {
      tinymce.init({
        target,
        menubar: false,
        statusbar: false,
        height: 500,
        theme: 'modern',
        language: 'ru',
        language_url: 'https://cdn.rawgit.com/kissarat/414faa9650e6de6a72a90a4b6cf42dd9/raw/b73b23f3ab8e4eb32b0244c6601d628416ccfa09/tinymce-ru.js',
        plugins: [
          'advlist autolink lists link image charmap preview hr anchor',
          'searchreplace visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
        ],
        toolbar1: 'insertfile undo redo | bold italic | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor | preview fullscreen code',
        image_advtab: true,
        templates: [
          {title: 'Test template 1', content: 'Test 1'},
          {title: 'Test template 2', content: 'Test 2'}
        ],
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css'
        ],
        setup: ed => ed.on('change', e => this.props.change(e.target.getContent()))
      })
    }
  }

  render() {
    return <textarea
      value={this.props.value}
      ref={this.textarea}/>
  }
}

export class ArticleEdit extends Form {
  componentWillReceiveProps(props) {
    this.setState({})
    if (props.params.id) {
      api.get('article/get', {id: props.params.id}).then(a => this.setState(a))
    }
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  pathFromName = () => {
    this.setState({path: translitURL(this.state.name)})
  }

  submit() {
    const params = this.props.params.id ? {id: this.props.params.id} : false
    this.send('article/save', params).then(({id}) => {
      if (!this.props.params.id) {
        browserHistory.push('/edit/' + id)
      }
    })
  }

  render() {
    let vb = ''
    let breadcrumb = [
      ['Статьи', '/articles']
    ]
    if (this.props.params.id) {
      const url = this.state.path ? '/' + this.state.path : '/view/' + this.props.params.id
      vb = <Link className="btn btn-primary" to={url}>Просмотр</Link>
      breadcrumb.push([this.state.name, url])
    }
    breadcrumb = Menu.normalize(breadcrumb)

    const price = 'product' === this.state.type ?
      <FieldGroup
        type="text"
        label="Стоимость"
        name="price"
        required
        value={this.state.price || ''}
        onChange={this.onChange}
      />
      : ''

    return <form className="article-editor" onSubmit={this.onSubmit}>
      <Menu className="breadcrumb" items={breadcrumb}/>
      <h1>Редактирование статьи</h1>
      <FieldGroup
        type="text"
        label="Название"
        name="name"
        required
        value={this.state.name || ''}
        onChange={this.onChange}
      />
      <FormGroup>
        <ControlLabel>Тип</ControlLabel>
        <select
          name="type"
          onChange={this.onChange}
          value={this.state.type || ''}
          className="form-control">
          <option value="page">Страница</option>
          <option value="product">Продукт</option>
          <option value="cat">Категория</option>
        </select>
      </FormGroup>
      {price}
      <FormGroup>
        <ControlLabel>Краткое описание</ControlLabel>
        <textarea
          name="short"
          maxLength="140"
          onChange={this.onChange}
          value={this.state.short || ''}
          className="form-control"/>
      </FormGroup>
      <FormGroup>
        <ControlLabel>Путь</ControlLabel>
        <div className="path">
          <input
            label="Путь"
            name="path"
            value={this.state.path || ''}
            onChange={this.onChange}
            className="form-control"/>
          <Button onClick={this.pathFromName}>Путь из названия</Button>
        </div>
      </FormGroup>
      <div className="form-group">
        <label>Текст</label>
        <RichEdit
          value={this.state.text || ''}
          change={v => this.setState({text: v})}
        />
      </div>
      <ButtonToolbar>
        <Button bsStyle="success" type="submit">Сохранить</Button>
        {vb}
      </ButtonToolbar>
    </form>
  }
}
