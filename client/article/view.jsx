import React, {Component} from 'react'
import api from '../connect/api.jsx'
import {browserHistory, Link} from 'react-router'
import {NotFound} from '../page.jsx'
import Busy from '../widget/busy.jsx'
import {map} from 'lodash'

export class View extends Component {
  componentWillReceiveProps(props) {
    // if ('' === props.params.splat) {
    //  browserHistory.push('/login')
    // }
    this.setState({})
    if (props.params) {
      const params = props.params.splat ? {path: props.params.splat} : props.params
      api.get('article/get', params)
        .then(a => this.setState({article: 404 === a.statusCode ? false : a}))
    }
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  render() {
    if (this.state.article) {
      const article = this.state.article
      let path = 'article'
      if (article.path) {
        path += ' ' + this.state.path
      }
      return <article className={path}>
        <h1 className="name">
          {article.name}
          <Link to={'/edit/' + article.id} className="fa fa-edit"/>
        </h1>
        <i className="short">{article.short}</i>
        <div className="text" dangerouslySetInnerHTML={{__html: article.text}}/>
      </article>
    }
    else {
      return <NotFound/>
    }
  }
}

export class List extends Component {
  componentWillReceiveProps(props) {
    this.setState({articles: false})
    api.get('article/index').then(articles => this.setState({articles}))
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  render() {
    let articles = map(this.state.articles, a => <li key={a.id} className={'item ' + a.path}>
      <Link to={a.path ? '/' + a.path : '/view/' + a.id}>
        <h2>{a.name}</h2>
        <p>{a.short}</p>
      </Link>
    </li>)
    articles = articles.length > 0
      ? <ul className="list">{articles}</ul>
      : <Busy>Загрузка статей...</Busy>

    return <article className="articles">
      <h1>Статьи</h1>
      <Link className="btn btn-primary" to="/create">Создать</Link>
      {articles}
    </article>
  }
}
