module.exports = {
  save(name, object) {
    localStorage.setItem(name, JSON.stringify(object))
  },

  load(name, initial = {}) {
    try {
      let object = localStorage.getItem(name)
      if (object) {
        return JSON.parse(object)
      }
    }
    catch (ex) {
    }
    return initial
  }
}
