import React, {Component} from 'react'
import {Link} from 'react-router'
import api from '../connect/api.jsx'
import Menu from '../widget/menu.jsx'

const menu = Menu.normalize([
  ['Кабинет', '/cabinet'],
  ['Рефералы', '/structure/referral'],
  ['Магазин', '/shop'],
  ['Финансы', '/finance'],
  ['Реклама', '/advertise'],
  ['Настройки', '/settings'],
  ['Информация', '/about'],
])

export default class Cabinet extends Component {
  componentWillMount() {
  }

  render() {
    return <div className="layout cabinet">
      <div className="container">
        <div className="navigation">
          <div className="logo">
            <img/>
          </div>
          <Menu items={menu}/>
        </div>
        <div className="content">{this.props.children}</div>
      </div>
    </div>
  }
}
