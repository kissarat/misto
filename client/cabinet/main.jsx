import React, {Component} from 'react'
import api from '../connect/api.jsx'

export default class Main extends Component {
  componentWillReceiveProps(props) {
    this.setState({})
    api.get(props.params.nick ? 'user/get' : 'user/me', props.params)
      .then(user => this.setState(user))
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  render() {
    return <div className="page main"><img src={this.state.avatar}/></div>
  }
}
