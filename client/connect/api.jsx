import 'whatwg-fetch'
import {isEmpty, each} from 'lodash'
// import {announce} from '../globals'
import qs from '../base/urlencoded.jsx'

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  Author: 'Taras Labiak <kissarat@gmail.com>'
}

class API {
  constructor() {
    this.prefix = '/serve'
  }

  getToken() {
    return localStorage.getItem('sam')
  }

  setToken(value) {
    localStorage.setItem('sam', value)
  }

  buildURL(url, params) {
    if ('/' !== url[0]) {
      const token = this.getToken()
      if (token) {
        url = `/~${token}/${url}`
      }
      else {
        url = '/' + url
      }
    }
    if (!isEmpty(params)) {
      url += '?' + qs.stringify(params)
    }
    return url
  }

  get(url, params) {
    url = this.prefix + this.buildURL(url, params)
    return fetch(url, {headers}).then(r => r.json())
  }

  async send(url, params, data) {
    if (!data) {
      data = params
      params = null
    }
    url = this.buildURL(url, params)
    const options = {
      method: 'POST',
      headers
    }
    if (!isEmpty(data) && true !== data) {
      options.body = JSON.stringify(data)
    }
    const json = (await fetch(this.prefix + url, options)).json()
    if (json.error) {
      this.error(json.error)
    }
    return json
  }

  get entities() {
    return this.config.entities
  }
}

const api = new API()
// announce({api})
module.exports = api
