import {each, isEmpty} from 'lodash'
import Emitter from '../base/emitter.jsx'
import api from './api.jsx'
import mixin from '../base/mixin.jsx'

const eventMethods = {
  create: 'onCreate',
  update: 'onUpdate',
  delete: 'onDelete',
  reset: 'onReset'
}

const Subscriber = mixin(Emitter, {
  constructor: function Subscriber(url, params) {
    Emitter.call(this)
    this.url = url
    this.params = params
  },

  subscribe(target) {
    each(eventMethods, (method, event) => {
      if ('function' === typeof target[method]) {
        this.on(event, r => target[method](r))
      }
    })
  },

  fetch(params) {
    if (!isEmpty(params)) {
      if (params.select instanceof Array) {
        params.select = params.select.join('.')
      }
    }
    return api.get(this.url, params)
  },

  change(params) {
    this.fetch(params).then(r => this.emit('reset', r))
  },

  start() {
    this.change(this.params)
  },

})

export default Subscriber
