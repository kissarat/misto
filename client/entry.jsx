import 'babel-polyfill'
import React from 'react'
import {Router, Route, IndexRedirect, browserHistory} from 'react-router'
import Admin from './admin/app.jsx'
import {View, List} from './article/view.jsx'
import {ArticleEdit} from './article/edit.jsx'
import Upload from './admin/upload.jsx'
import {render} from 'react-dom'
import api from './connect/api.jsx'
import {Unavailable, Development} from './page.jsx'
import Outdoor from './outdoor/app.jsx'
import Login from './outdoor/login.jsx'
import Signup from './outdoor/signup.jsx'
import Cabinet from './cabinet/app.jsx'
import Main from './cabinet/main.jsx'
import FirmList from './firm/index.jsx'

export const routes = <Route path='/'>
  <IndexRedirect to='/login'/>
  <Route component={Outdoor}>
    <Route path='login' component={Login}/>
    <Route path='signup' component={Signup}/>
    <Route path='ref/:id' component={Signup}/>
  </Route>
  <Route component={Cabinet}>
    <Route path="cabinet" component={Main} />
    <Route path='view/:id' component={View}/>
  </Route>
  <Route component={Admin}>
    <Route path="articles" component={List} />
    <Route path="create" component={ArticleEdit} />
    <Route path="edit/:id" component={ArticleEdit} />
    <Route path="upload" component={Upload} />
  </Route>
  <Route path="firms" component={FirmList} />
  <Route path='*' component={View}/>
</Route>

export const router = <Router history={browserHistory}>{routes}</Router>

const params = {name: navigator.userAgent, expires: new Date(2030, 0, 1, 2, 0).toISOString()}

api.send('handshake/' + Date.now(), null, params)
  .then(function (config) {
    if (config.token) {
      api.setToken(config.token.id)
    }
    api.config = config
    render(config.maintenance ? <Development/> : router, document.getElementById('app'))
  })
  .catch(function (err) {
    console.error(err)
    render(<Unavailable/>, document.getElementById('app'))
  })
