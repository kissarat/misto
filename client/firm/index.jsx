import React, {Component} from 'react'
import {map, pick, defaults, debounce, keyBy} from 'lodash'
import api from '../connect/api.jsx'

export class FirmCard extends Component {
  render() {
    return <div className="widget firm card">
      <h2>{this.props.name}</h2>
      <div>
        <div>{this.props.email}</div>
        <div>{this.props.site || ''}</div>
        <div>{this.props.text || ''}</div>
      </div>
    </div>
  }
}

export default class FirmList extends Component {
  componentWillReceiveProps(props) {
    // this.fetch()
  }

  componentWillMount() {
    this.setState({
      firms: [],
      search: ''
    })
    this.componentWillReceiveProps(this.props)
  }

  fetch = () => {
    const page = defaults(pick(this.state, 'offset', 'limit', 'search'), {
      offset: 0,
      limit: 100
    })
    api.get('firm/index', page).then(firms => {
      const c = this.state.firms
      firms.forEach(function (firm) {
        c[firm.id] = firm
      })
      this.setState({firms: c})
    })
  }

  onSearch = (e) => {
    this.setState({search: e.target.value})
    debounce(this.fetch, 400)
  }

  render() {
    const list = map(this.state.firms, (f, id) => <li key={id}><FirmCard {...f}/></li>)
    return <div className="page firm">
      <input type="search" value={this.state.value} onKeyUp={this.onSearch} />
      <ul>{list}</ul>
    </div>
  }
}
