const isClient = !!('undefined' !== typeof window && window.document)

exports.global = isClient ? window : global
exports.isClient = isClient
exports.isServer = !isClient
exports.isChrome = isClient && !!self.chrome && !!window.chrome.webstore;
exports.isBlink = isClient && exports.isChrome && !!window.CSS;
exports.isFirefox = isClient && self.InstallTrigger;

exports.DEBUG = true
Object.defineProperty(exports, 'DEBUG', {
  writable: false,
  value: !!('undefined' !== typeof localStorage && +localStorage.getItem('debug'))
})

exports.debug = exports.DEBUG ? console.log.bind(console) : Function()

exports.announce = function announce(objects) {
  for (var name in objects) {
    Object.defineProperty(exports, name, {
      writable: true,
      value: objects[name]
    })
  }
}
