import React, {Component} from 'react'

export default class Outdoor extends Component {
  render() {
    return <div className="layout outdoor">
      <div className="container">
        <div className="left"></div>
        <div className="right">{this.props.children}</div>
      </div>
    </div>
  }
}
