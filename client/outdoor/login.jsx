import React, {Component} from 'react'
import {FieldGroup} from '../widget/field.jsx'
import {ButtonToolbar, Button} from 'react-bootstrap'
import Form from '../widget/form.jsx'
import {Link, browserHistory} from 'react-router'

export default class Login extends Form {
  componentWillReceiveProps() {
    this.setState({})
  }

  componentWillMount() {
    this.componentWillReceiveProps()
  }

  submit() {
    this.send('user/login/' + this.state.nick).then(data => {
      this.clearPrivate()
      if (data.success) {
        browserHistory.push('/cabinet')
      }
      else if ('ABSENT' === data.status) {
        this.setHelp(data.error.name, 'Неправильно')
      }
      else {
        alert('Неизвестная ошибка')
      }
    })
  }

  render() {
    return <form className="page login" onSubmit={this.onSubmit}>
      <div>
        <h1>Вхід</h1>
        <h2>Введіть логін и пароль для входу</h2>
        <FieldGroup
          name="nick"
          type="text"
          label="Логін"
          required
          pattern="^[\w_]+$"
          title="Допускаються латинські букви, числа та символ _"
          value={this.state.nick || ''}
          onChange={this.onChange}
          help={this.state._nick}
        />
        <FieldGroup
          name="password"
          type="password"
          label="Пароль"
          required
          pattern="^.{6,128}$"
          title="Введіть більше 6-ти і менше 128-ка символів"
          value={this.state.password || ''}
          onChange={this.onChange}
          help={this.state._password}
        />
        <ButtonToolbar>
          <Button type="submit">Ввійти</Button>
          <Link to='/signup'>Реєстрація</Link>
          <Link to='/recovery'>Забули пароль?</Link>
        </ButtonToolbar>
      </div>
      <div className="important">
        <div className="title">Важливо</div>
        <div className="message">Ніколи не передавайте пароль</div>
      </div>
    </form>
  }
}
