import React, {Component} from 'react'
import {FieldGroup} from '../widget/field.jsx'
import {ButtonToolbar, Button, FormControl, FormGroup, ControlLabel, HelpBlock} from 'react-bootstrap'
import Form from '../widget/form.jsx'
import {Link, browserHistory} from 'react-router'

export default class Login extends Form {
  componentWillReceiveProps(props) {
    this.setState({})
    if (props.params.id) {
      localStorage.setItem('ref', props.params.id)
    }
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  submit() {
    this.send('user/signup/' + this.state.nick).then(data => {
      this.clearPrivate()
      if (data.success) {
        browserHistory.push('/cabinet')
      }
      else if ('INVALID_UNIQUE' === data.status) {
        this.setHelp(data.error.name, 'Користувач вже зареєстрований')
      }
      else {
        console.log(data)
      }
    })
  }

  render() {
    return <form className="page signup" onSubmit={this.onSubmit}>
      <h1>Реєстрація</h1>
      <h2>Данные шифруются і ніколи не передаются іншим особам</h2>
      <FieldGroup
        name="nick"
        type="text"
        label="Логін"
        required
        pattern="^[\w_]+$"
        title="Допускаються латинські букви, числа та символ _"
        value={this.state.nick || ''}
        help={this.state._nick}
        onChange={this.onChange}
      />
      <FieldGroup
        name="password"
        type="password"
        label="Пароль"
        required
        pattern="^.{6,128}$"
        title="Введіть більше 6-ти і менше 128-ка символів"
        value={this.state.password || ''}
        help={this.state._password}
        onChange={this.onChange}
      />
      <FieldGroup
        name="email"
        type="email"
        label="Email"
        required
        value={this.state.email || ''}
        help={this.state._email}
        onChange={this.onChange}
      />
      <ControlLabel>
        <FormControl type="checkbox" name="agree" required/>
        <span>Погоджуюсь з <a href="https://dimakovpak.com/oferta.pdf" target="_blank">Умовами використання</a></span>
      </ControlLabel>
      <ButtonToolbar>
        <Button type="submit">Зареєструватись</Button>
        <Link to='/login'>Ввійти</Link>
        <Link to='/recovery'>Забули пароль?</Link>
      </ButtonToolbar>
    </form>
  }
}
