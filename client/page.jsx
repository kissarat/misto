import React, {Component} from 'react'

export class NotFound extends Component {
  render() {
    return <div className="page error not-found">
      <img
        src="http://vk-mm.com/images/not-found.png"
        alt="Страница не найдена"
      />
    </div>
  }
}

export class Unavailable extends Component {
  render() {
    return <div className="page error unavailable">
      <img
        src="http://vk-mm.com/images/unavailable.jpg"
        alt="Сервер недоступен"
      />
    </div>
  }
}

export class Development extends Component {
  render() {
    return <div className="page error development">
      <img
        src="http://vk-mm.com/images/development.png"
        alt="Случилась ошибка, попробуйте загрузить страницу позже"
      />
    </div>
  }
}
