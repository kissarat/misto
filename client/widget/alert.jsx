import React, {Component} from 'react'
import {map} from 'lodash'
import {announce} from '../globals'

export class Alert extends Component {
  render() {
    return <div className={'alert alert-' + this.props.type}>{this.props.text}</div>
  }
}

export class AlertList extends Component {
  componentWillMount() {
    this.setState({})
  }

  render() {
    const alerts = map(this.state, o => <Alert {...o}/>)
    return <div id="alert-list">{alerts}</div>
  }

  show(options) {
    this.setState({[options.id || (options.type + Date.now())]: options})
  }
}

AlertList.instance = new AlertList()

export default AlertList.instance

announce({AlertList})
