import React, {Component} from 'react'

const Busy = ({children}) => <div className="busy">
  <img src="http://vk-mm.com/images/busy.svg"/>
  {children}
</div>

export default Busy
