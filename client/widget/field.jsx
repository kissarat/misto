import React, {Component} from 'react'
import {FormControl, FormGroup, ControlLabel, HelpBlock} from 'react-bootstrap'

export function FieldGroup(props) {
  return <FormGroup controlId={props.id}>
      <ControlLabel>{props.label}</ControlLabel>
      <FormControl {...props} />
      {props.help && <HelpBlock>{props.help}</HelpBlock>}
    </FormGroup>
}
