import React, {Component} from 'react'
import api from '../connect/api.jsx'

export default class Form extends Component {
  onChange = (e) => {
    const name = e.target.getAttribute('name')
    const value = e.target.value
    this.setState({[name]: value})
    // if (this.change instanceof Function) {
    //   this.change(name, value)
    // }
  }

  onSubmit = (e) => {
    e.preventDefault()
    this.submit()
  }

  clearPrivate() {
    const privates = {}
    for(const key in this.state) {
      if ('_' === key[0]) {
        privates[key] = null
      }
    }
    this.setState(privates)
  }

  setHelp(name, message) {
    this.setState({['_' + name]: message})
  }

  send(url, params) {
    const data = {}
    for(const key in this.state) {
      if ('_' !== key[0]) {
        data[key] = this.state[key]
      }
    }
    return params
      ? api.send(url, params, data)
      : api.send(url, data)
  }
}
