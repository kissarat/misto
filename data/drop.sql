DROP VIEW IF EXISTS "token_user";

DROP TABLE IF EXISTS "good";
DROP TABLE IF EXISTS "firm";
DROP TABLE IF EXISTS "image";
DROP TABLE IF EXISTS "request";
DROP TYPE "request_method";
DROP TABLE IF EXISTS "log";
DROP TABLE IF EXISTS "social";
DROP TABLE IF EXISTS "visit";
DROP TABLE IF EXISTS "token";
DROP TYPE "token_type";
DROP INDEX "user_nick";
DROP INDEX "user_email";
DROP TABLE IF EXISTS "user";
DROP TABLE IF EXISTS "week";
DROP TABLE IF EXISTS "attr";
DROP TYPE "attr_type";
DROP TABLE IF EXISTS "relation";
DROP TYPE "relation_type";
DROP TABLE IF EXISTS "cat";
DROP TABLE IF EXISTS "node";
DROP TYPE "node_type";
