const data = require('./export.json')
const {map, values} = require('lodash')

const a = map(data, function (rows, name) {
  return rows.map(function (row) {
    const k = Object.keys(row).map(a => `"${a}"`)
    const v = values(row).map(a => `$$${a}$$`).join(',')
    console.log(`INSERT INTO ${name}(${k}) values (${v});`)
  })
    .join('\n')
})
  .join('\n')

console.log(a)
