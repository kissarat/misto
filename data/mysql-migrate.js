const knex = require('knex')
const config = require('../config')
const mysql = knex(config.mysql)
const db = knex(config.database)
const fs = require('fs-extra')
const {extend} = require('lodash')

function seq(promises) {
  return promises.length > 1
    ? promises[0].then(() => seq(promises.slice(1)))
    : promises[0]
}

const jsonData = {}

function migrate({from, to, columns, parse, constants}) {
  const jsonTable = jsonData[to] = []
  return mysql.table(from).select(Object.keys(columns))
    .then(rows => db.table(to).insert(rows.map(inRow => {
      const outRow = {}
      for (const key in inRow) {
        let value = inRow[key]
        if (('string' === typeof value) && !value.trim()) {
          value = null
        }
        outRow[columns[key]] = value
      }
      if (parse instanceof Function) {
        parse(outRow)
      }
      extend(outRow, constants)
      jsonTable.push(outRow)
      if (outRow.text instanceof Buffer) {
        outRow.text = outRow.text.toString('utf8')
      }
      return outRow
    })))
}

function trim(str, apx) {
  if (str) {
    if (apx)
      str = str.replace(apx, '');
    return str.trim();
  }
  return null;
}

function parseNumber(number) {
  number = trim(number);
  if (number) {
    var num_abc = /(\d+) *([абв])/i.exec(number);
    if (num_abc)
      number = num_abc[1] + num_abc[2].toLowerCase();
    else {
      number = /(\d+)/.exec(number);
      if (number)
        number = number[1];
    }
    return number;
  }
  return null;
}

const regex = Object.freeze({
  city: /(м\.|смт\.|с\.)/,
  street: /(вул\.|бульв\.|просп\.|про|пров\.|майдан)/
})

const migrations = [
  {
    from: 'firms',
    to: 'firm',
    columns: {
      firm_id: 'fid',
      mark: 'rank',
      name: 'name',
      address: 'address',
      phone: 'phone',
      email: 'email',
      homepage: 'site',
      advertise: 'text',
      mapg_x: 'lat',
      mapg_y: 'log'
    },
    parse(row) {
      // if ('string' === typeof row.address) {
      //   let address = row.address.split(';')
      //   if (address.length > 1) {
      //     let city = address[1].replace('м.', '')
      //     let area = address[1].replace('район', '')
      //     if (city.length != address[1].length) {
      //       row.city = trim(city)
      //       address = address[3] ? address[3].split(',') : address.slice(2)
      //       row.street = trim(address[0], regex.street)
      //       row.house = parseNumber(address[1])
      //       row.parsed = true
      //     }
      //     else if (area.length != address[1].length) {
      //       row.area = trim(area)
      //       address = address[2].split(',')
      //       row.city = trim(address[0], regex.city)
      //       row.street = trim(address[1], regex.street)
      //       row.house = parseNumber(address[2])
      //     }
      //   }
      // }
    }
  },

  {
    from: 'rubricator',
    to: 'cat',
    columns: {
      rubricator_bit: 'cid',
      parent: 'parent',
      name: 'name'
    }
  },

  {
    from: 'goods',
    to: 'good',
    columns: {
      goods_id: 'gid',
      rubricator_bit: 'cat',
      name: 'name'
    }
  },

  {
    from: 'firm_cat',
    to: 'relation',
    columns: {
      firm: 'left',
      cat: 'right'
    },
    constants: {
      type: 'is'
    }
  },

  {
    from: 'firm_good',
    to: 'relation',
    columns: {
      firm: 'left',
      good: 'right'
    },
    constants: {
      type: 'is'
    }
  }
]

seq(migrations.map(migrate))
  .then(function () {
    console.log('Migration successful')
    fs.writeJsonSync(__dirname + '/export.json', jsonData)
    process.exit(0)
  })
  .catch(function (err) {
    console.error(err)
    fs.writeJsonSync(__dirname + '/export.json', jsonData)
    process.exit(1)
  })
