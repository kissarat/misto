CREATE VIEW firm_cat AS
  SELECT
    firm_id        AS firm,
    rubricator_bit AS cat
  FROM prices;

CREATE VIEW firm_good AS
  SELECT
    firm_id  AS firm,
    goods_id AS good
  FROM prices;
