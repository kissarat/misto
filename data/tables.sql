CREATE TYPE node_type AS ENUM ('cat', 'article', 'good', 'firm');

CREATE TABLE node (
  id       SERIAL PRIMARY KEY NOT NULL,
  path     VARCHAR(96) UNIQUE,
  "type"   node_type          NOT NULL DEFAULT 'article',
  name     VARCHAR(96)        NOT NULL,
  short    VARCHAR(160),
  text     TEXT,
  keywords TEXT,
  comment  TEXT,
  created  TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TYPE relation_type AS ENUM ('is', 'has');

CREATE TABLE relation (
  id      SERIAL PRIMARY KEY NOT NULL,
  "left"  INT REFERENCES "node" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "right" INT REFERENCES "node" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "type"  relation_type      NOT NULL
);

CREATE TYPE attr_type AS ENUM ('site', 'email', 'fax');

CREATE TABLE attr (
  number SMALLINT  NOT NULL DEFAULT 0,
  "node" INT REFERENCES "node" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  type   attr_type NOT NULL,
  value  VARCHAR(256)
);

CREATE TABLE week (
  "node" INT REFERENCES "node" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  period INT4RANGE NOT NULL
);

CREATE TABLE "user" (
  nick      VARCHAR(24) NOT NULL CHECK (nick ~ '^[\w_]+$'),
  email     VARCHAR(48) NOT NULL CHECK (email ~ '^.*@.*\.\w+'),
  "admin"   BOOLEAN     NOT NULL DEFAULT FALSE,
  skype     VARCHAR(32),
  avatar    VARCHAR(192),
  surname   VARCHAR(48),
  forename  VARCHAR(48),
  gavatar   INT,
  vkontakte INT,
  facebook  VARCHAR(24),
  google    VARCHAR(24)
)
  INHERITS (node);
CREATE UNIQUE INDEX user_nick
  ON "user" (lower(nick));
CREATE UNIQUE INDEX user_email
  ON "user" (lower(email));

CREATE TYPE token_type AS ENUM ('plain', 'password', 'vkontakte', 'facebook', 'google');

CREATE TABLE token (
  id        VARCHAR(240) PRIMARY KEY,
  "node"    INT REFERENCES "node" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "type"    token_type NOT NULL DEFAULT 'plain',
  created   TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time      TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  handshake TIMESTAMP,
  expires   TIMESTAMP  NOT NULL,
  name      VARCHAR(240)
);

CREATE TABLE visit (
  time  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip    INET         NOT NULL,
  token VARCHAR(240) NOT NULL REFERENCES token (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  node  INT          NOT NULL REFERENCES "node" (id)
  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE social (
  id      BIGSERIAL PRIMARY KEY,
  type    token_type NOT NULL,
  created TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  data    JSON       NOT NULL
);

CREATE TABLE log (
  id     BIGINT PRIMARY KEY,
  entity VARCHAR(16) NOT NULL,
  action VARCHAR(32) NOT NULL,
  ip     INET,
  "user" INT REFERENCES "node" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  data   JSON
);

CREATE TYPE request_method AS ENUM ('OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT');

CREATE TABLE request (
  id     BIGSERIAL PRIMARY KEY,
  time   TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip     INET,
  url    VARCHAR(4096)  NOT NULL,
  method request_method NOT NULL,
  type   VARCHAR(64),
  agent  VARCHAR(512),
  "data" TEXT
);

CREATE TABLE image (
  id         VARCHAR(24) PRIMARY KEY,
  height     SMALLINT    NOT NULL,
  width      SMALLINT    NOT NULL,
  created    TIMESTAMP   NOT NULL,
  type       VARCHAR(48) NOT NULL,
  deletehash VARCHAR(24) NOT NULL,
  link       VARCHAR(96),
  size       INT         NOT NULL,
  account_id INT         NOT NULL,
  animated   BOOLEAN     NOT NULL
);

CREATE TABLE "firm" (
  "fid"     BIGINT,
  "address" VARCHAR(255),
  "phone"   VARCHAR(128),
  "email"   VARCHAR(128),
  "site"    VARCHAR(128),
  "working" VARCHAR(128),
  "rank"    INT     NOT NULL,
  "number"  SMALLINT,
  "city"    VARCHAR(48),
  "area"    VARCHAR(16),
  "street"  VARCHAR(64),
  "house"   VARCHAR(4),
  "lat"     DOUBLE PRECISION CHECK (lat >= -90 AND lat <= 90),
  "log"     DOUBLE PRECISION CHECK (log > -180 AND log <= 180),
  "parsed"  BOOLEAN NOT NULL DEFAULT FALSE
)
  INHERITS (node);

CREATE TABLE "good" (
  "gid"   BIGINT,
  "price" NUMERIC(8, 2)
)
  INHERITS (node);

CREATE TABLE "cat" (
  "cid" BIGINT
)
  INHERITS (node);
