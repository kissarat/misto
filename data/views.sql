CREATE VIEW token_user AS
  SELECT
    t.id,
    t.type,
    t.name,
    row_to_json(n.*) AS "node"
  FROM token t
    LEFT JOIN "node" n ON t.node = n.id
  WHERE expires > CURRENT_TIMESTAMP;
