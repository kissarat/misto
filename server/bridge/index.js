const {Router} = require('express')
const config = require('../../config')
const {each} = require('lodash')
const passport = require('passport')

const router = Router()

router.get('/', function (req, res) {
  const bridges = []
  each(config.bridge, function ({enabled}, name) {
    if (enabled) {
      bridges.push(name)
    }
  })
  res.json({
    time: new Date().toISOString(),
    bridges
  })
})

each(config.bridge, function (thisConfig, name) {
  if (thisConfig.enabled) {
    const module = require('./' + name)
    const url = '/' + name
    thisConfig.callbackURL = config.origin + `/serve/bridge/${name}/callback`
    module.register(thisConfig)
    if (module.router instanceof Function) {
      router.use(url, module.router())
    }
  }
})

module.exports = {router}
