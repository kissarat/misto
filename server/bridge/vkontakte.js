const passport = require('passport')
const {Strategy} = require('passport-vkontakte')
const {Router} = require('express')

const vk = {
  permissions: {
    ads: 'Доступ к расширенным методам работы с <a href="/pages?oid=-1&amp;p=Ads_API">рекламным API</a>.',
    audio: 'Доступ к аудиозаписям.',
    docs: 'Доступ к документам.',
    friends: 'Доступ к друзьям.',
    groups: 'Доступ к группам пользователя.',
    //messages: '<b>(для Standalone-приложений)</b> Доступ к расширенным методам работы с сообщениями.',
    //nohttps: 'Возможность осуществлять <a href="/pages?oid=-1&amp;p=%D0%92%D0%B7%D0%B0%D0%B8%D0%BC%D0%BE%D0%B4%D0%B5%D0%B9%D1%81%D1%82%D0%B2%D0%B8%D0%B5_%D1%81_API_%D0%B1%D0%B5%D0%B7_HTTPS">запросы к API без HTTPS</a>.<br>↵ <b>Внимание, данная возможность находится на этапе тестирования и может быть изменена.</b>',
    notes: 'Доступ заметкам пользователя.',
    notifications: 'Доступ к оповещениям об ответах пользователю.',
    notify: 'Пользователь разрешил отправлять ему уведомления.',
    offline: 'Доступ к <a href="/pages?oid=-1&amp;p=API">API</a> в любое время со стороннего сервера.',
    pages: 'Доступ к wiki-страницам.',
    photos: 'Доступ к фотографиям.',
    stats: 'Доступ к статистике групп и приложений пользователя, администратором которых он является.',
    status: 'Доступ к статусу пользователя.',
    video: 'Доступ к видеозаписям.',
    // wall: 'Доступ к обычным и расширенным методам работы со стеной.<br><b>Внимание, данное право доступа недоступно для сайтов (игнорируется при попытке авторизации).</b>'
  },
  errors: {
    1: 'Произошла неизвестная ошибка.',
    2: 'Приложение выключено.',
    3: 'Передан неизвестный метод.',
    4: 'Неверная подпись.',
    5: 'Авторизация пользователя не удалась.',
    6: 'Слишком много запросов в секунду.',
    7: 'Нет прав для выполнения этого действия.',
    8: 'Неверный запрос.',
    9: 'Слишком много однотипных действий.',
    10: 'Произошла внутренняя ошибка сервера.',
    11: 'В тестовом режиме приложение должно быть выключено или пользователь должен быть залогинен.',
    14: 'Требуется ввод кода с картинки (Captcha).',
    15: 'Доступ запрещён.',
    16: 'Требуется выполнение запросов по протоколу HTTPS, т.к. пользователь включил настройку, требующую работу через безопасное соединение.',
    17: 'Требуется валидация пользователя.',
    20: 'Данное действие запрещено для не Standalone приложений.',
    21: 'Данное действие разрешено только для Standalone и Open API приложений.',
    23: 'Метод был выключен.',
    24: 'Требуется подтверждение со стороны пользователя.',
    100: 'Один из необходимых параметров был не передан или неверен.',
    101: 'Неверный API ID приложения.',
    113: 'Неверный идентификатор пользователя.',
    150: 'Неверный timestamp',
    200: 'Доступ к альбому запрещён.',
    201: 'Доступ к аудио запрещён.',
    203: 'Доступ к группе запрещён.',
    300: 'Альбом переполнен.',
    500: 'Действие запрещено. Вы должны включить переводы голосов в настройках приложения.',
    600: 'Нет прав на выполнение данных операций с рекламным кабинетом.',
    603: 'Произошла ошибка при работе с рекламным кабинетом.'
  },

  profile: {
    fields: [
      'verified', 'sex', 'bdate', 'city', 'country', 'home_town',
      'photo_max', 'online', 'has_mobile', 'contacts',
      'site', 'last_seen', 'followers_count',
      'common_count', 'nickname', 'relatives', 'relation',
      'connections', 'exports', 'can_send_friend_request',
      'timezone', 'screen_name', 'maiden_name', 'domain',
      'about'
    ].join(',')
  },

  friends: {
    order: 'hints',
    count: 1000,
    fields: ['domain', 'bdate', 'sex', 'city', 'country'].join(',')
  }
};

function callback(accessToken, refreshToken, profile, done) {
  console.log(accessToken, refreshToken, profile)
  done()
}

function handle(req, res) {
  res.json({
    params: req.query
  })
}

module.exports = {
  callback, handle,

  register(config) {
    passport.use(new Strategy(config, callback))
  },

  router() {
    const router = Router()
    router.get('/', passport.authenticate('vkontakte', {scope: Object.keys(vk.permissions)}), handle)
    router.get('/callback',
      passport.authenticate('vkontakte', {
        successRedirect: '/cabinet',
        failureRedirect: '/login'
      })
    );
    return router
  }
}
