const config = require('../config')
const db = require('schema-db')

module.exports = db
db.connect = function () {
  return db.setup(config.database)
}
