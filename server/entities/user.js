const {extend} = require('lodash')

class User {
  constructor(object) {
    if (object) {
      extend(this, object)
    }
    else {
      this.guest = true
    }
  }

  toString() {
    return this.nick || this.id || 'guest'
  }

  getName() {

  }
}

module.exports = User
