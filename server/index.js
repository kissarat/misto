const express = require('express')
const middleware = require('./middleware')
const db = require('./db')
const {extend} = require('lodash')
const {IncomingMessage} = require('http')
const article = require('./modules/article')
const {router} = require('./bridge')

extend(IncomingMessage.prototype, require('./request'))

const app = express()

app.use(middleware.authentication)
app.use(middleware.log)
app.use('/bridge', router)
app.post('/handshake/:time', middleware.handshake)
app.use(middleware.handle)
app.get('/view/:path', article.view)
app.use(middleware.notFound)

function run() {
  db.connect()
    .then(function () {
      app.listen(8002, function () {
        process.title = 'node-sam'
      })
    })
    .catch(function (err) {
      console.error(err)
      process.exit(1)
    })
}

if (module.parent) {
  module.exports = run
}
else {
  run()
}
