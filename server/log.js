const db = require('schema-db')
const body = require('body')
const salo = require('salo')
const qs = require('querystring')
const config = require('../config')

module.exports = function logger(req, res, next) {
  body(req, config.data, function (err, data) {
    if (err) {
      res
        .status(400)
        .json(salo(err))
    }
    else {
      const record = {
        url: req.url,
        method: req.method,
        ip: req.headers.ip,
        type: req.headers['content-type'],
        agent: req.headers['user-agent'],
        data: data || null
      }
      if (data && record.type) {
        if (record.type.indexOf('application/json') >= 0) {
          try {
            req.body = JSON.parse(data)
          }
          catch (ex) {
            return res
              .status(400)
              .json(salo(ex))
          }
        }
        else if (record.type.indexOf('application/x-www-form-urlencoded') >= 0) {
          try {
            req.body = qs.parse(data)
          }
          catch (ex) {
            return res
              .status(400)
              .json(salo(ex))
          }
        }
      }
      if (/^.((user.(login|signup))|perfect|handshake|(bridge.\w+.callback))/.test(req.url)) {
        db.table('request').insert(record)
          .then(() => next())
          .catch(salo.express(res))
      }
      else {
        next()
      }
    }
  })
}
