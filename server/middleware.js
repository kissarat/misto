const salo = require('salo')
const db = require('./db')
const {pick, isObject, isEmpty} = require('lodash')
const Token = require('./entities/token')
const User = require('./entities/user')
const modules = require('./modules')

module.exports = {
  log: require('./log'),

  notFound(req, res) {
    const text = [
      req.url,
      'Not Found',
      'Страница не найдена',
      '\n',
      req.headers['user-agent'],
      new Date().toISOString(),
      req.headers.ip
    ]
      .join('\n')

    res
      .status(404)
      .type('text')
      .send(text)
  },

  handshake(req, res) {
    const data = modules.home.config()
    if (req.token.id) {
      req.token.update({handshake: new Date()})
        .then(function () {
          data.token = pick(req.token, 'id', 'name', 'expires')
          data.user = req.user
          res.json(data)
        })
        .catch(salo.express(res))
    }
    else {
      modules.token.create.call(req, req.body)
        .then(function (token) {
          data.token = token
          res.json(data)
        })
        .catch(salo.express(res))
    }
  },

  authentication(req, res, next) {
    function assign(token, user) {
      req.token = new Token(token)
      req.user = new User(user)
    }

    const url = req.url.split('/')
    if ('~' === url[1][0]) {
      req.url = '/' + url.slice(2).join('/')
      const token = url[1].slice(1)
      db.table('token_user')
        .where('id', token)
        .then(function (tokens) {
          if (tokens.length > 0) {
            assign(tokens[0], tokens[0].user)
          }
          else {
            assign()
            /*
            res
              .status(403)
              .json({
                statusCode: 403,
                status: 'TOKEN_NOT_FOUND',
                url: req.url,
                token,
                error: {
                  message: 'Token not found'
                }
              })
              */
          }
          next()
        })
    }
    else {
      assign()
      next()
    }
  },

  handle(req, res, next) {
    function json(data) {
      if (isObject(data)) {
        if (data.statusCode) {
          res.status(data.statusCode)
        }
        res.json(data)
      }
      else {
        res.json({
          statusCode: 404,
          error: {message: `${data} is not object`}
        })
      }
    }

    let [_1, module, action] = req.path.split('/')
    if (isObject(modules[module])) {
      module = modules[module]
      action = module[action]
      if (action instanceof Function) {
        try {
          const result = action.call(req, isEmpty(req.query) ? req.body : req.query, req.body)
          if (result) {
            if (result.then instanceof Function) {
              result
                .then(json)
                .catch(salo.express(res))
            }
            else {
              json(result)
            }
          }
          else {
            res
              .status(202)
              .json({status: 'ACCEPTED'})
          }
        }
        catch (ex) {
          res
            .status(500)
            .json(salo(ex))
        }
      }
      else {
        next()
      }
    }
    else {
      next()
    }
  }
}
