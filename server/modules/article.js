const db = require('schema-db')
const {pick} = require('lodash')
const salo = require('salo')

module.exports = {
  get(params) {
    return db.table('article')
      .where(pick(params, 'id', 'path'))
      .then(([a]) => a || {statusCode: 404})
  },

  save({id}, data) {
    let q = db.table('article')
    q = id
      ? q.where('id', id).update(data, 'id')
      : q.insert(data, 'id')
    return q.then(r => ({id: r[0]}))
  },

  view(req, res) {
    this.get(req.params)
      .then(function (a) {
        if (isFinite(a.statusCode)) {
          res.status(a.statusCode)
        }
        res.json(a)
      })
      .catch(salo.express)
  },

  index(params = {}) {
    params.select = ['id', 'path', 'parent', 'type', 'name', 'short']
    return db.entities.article.read(params)
  }
}
