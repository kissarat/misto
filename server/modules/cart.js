const db = require('schema-db')
const salo = require('salo')

module.exports = {
  index(params = {}) {
    if (this.user.id) {
      params.select = ['id']
      if (!params.payment) {
        params.select.push('payment')
      }
      if (this.user.admin) {
        params.select.push('owner')
      }
      else {
        params.owner = this.user.id
      }
      return db.entities.cart.read(params)
    }
    else {
      return {statusCode: 401}
    }
  }
}
