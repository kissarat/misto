const db = require('schema-db')
const {pick} = require('lodash')
const salo = require('salo')

module.exports = {
  get(params) {
    return db.table('firm')
      .where(params)
      .then(([a]) => a || {statusCode: 404})
  },

  index(params = {}) {
    return db.entities.firm.read(params)
  }
}
