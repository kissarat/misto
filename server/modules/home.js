const config = require('../../config')
const {pick} = require('lodash')

module.exports = {
  config() {
    const data = pick(config, 'maintenance')
    const time = process.hrtime()
    data.uptime = time[0] + time[1]/1000000000
    data.start = new Date(Date.now() - data.uptime * 1000).toISOString()
    return data
  },

  mirror(query, body) {
    return {
      headers: this.headers,
      params: this.params,
      query,
      body
    }
  }
}
