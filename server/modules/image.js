const db = require('schema-db')
const {pick} = require('lodash')
const salo = require('salo')

module.exports = {
  get(params) {
    return db.table('image')
      .where(params)
      .then(([a]) => a || {statusCode: 404})
  },

  create(data) {
    data = pick(data, 'id', 'height', 'width', 'datetime', 'type', 'deletehash',
    'link', 'size', 'account_id', 'animated')
    if (data.datetime) {
      data.created = new Date(data.datetime * 1000)
      delete data.datetime
    }
    return db.table('image')
      .insert(data, Object.keys(data))
      .then(([image]) => image)
  },

  view(req, res) {
    this.get(req.params)
      .then(function (a) {
        if (isFinite(a.statusCode)) {
          res.status(a.statusCode)
        }
        res.json(a)
      })
      .catch(salo.express)
  },

  index(params = {}) {
    return db.entities.image.read(params)
  }
}
