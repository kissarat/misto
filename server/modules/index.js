module.exports = {
  article: require('./article'),
  cart: require('./cart'),
  firm: require('./firm'),
  home: require('./home'),
  image: require('./image'),
  item: require('./item'),
  token: require('./token'),
  user: require('./user'),
}
