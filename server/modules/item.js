const db = require('schema-db')
const salo = require('salo')

module.exports = {
  add(params = {}) {
    if (this.user.guest) {
      return {statusCode: 401}
    }
    else {
      return db.table('cart')
        .where('owner', this.user.id)
        .where(db.raw('payment is null'))
        .then(carts => {
          return carts.length > 0
            ? carts
            : db.table('cart').insert({owner: this.user.id}, ['id'])
        })
        .then(function ([cart]) {
          params.cart = cart.id
          return db.table('item').insert(params, ['cart', 'article'])
        })
        .then(([item]) => item)
    }
  },

  index(params = {}) {
    if (this.user.guest) {
      return {statusCode: 401}
    }
    else {
      params.select = ['article']
      if (!params.payment) {
        params.select.push('payment')
      }
      if (this.user.admin) {
        params.select.push('owner')
      }
      else {
        params.owner = this.user.id
      }
      return db.entities.cart_item.read(params)
    }
  }
}
