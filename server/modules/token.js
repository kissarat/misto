const db = require('schema-db')
const config = require('../../config')
const rs = require('randomstring')
const {pick} = require('lodash')

function generateToken() {
  return rs.generate({
    length: 24,
    charset: 'alphabetic'
  })
}

module.exports = {
  create(params) {
    params.id = generateToken()
    return db.table('token').insert(pick(params, 'id', 'expires', 'name'), ['id', 'time', 'expires', 'name'])
      .then(([r]) => {
        return this.log('token', 'delete', r).then(function () {
          r.success = true
          return r
        })
      })
  },

  reset({id}, params) {
    params.id = generateToken()
    params.time = new Date().toISOString()
    return db.table('token')
      .where('id', id)
      .update(pick(params, 'id', 'time', 'expires', 'name'), ['id', 'time', 'created', 'expires', 'name'])
      .then(([r]) => {
        if (r) {
          return this.log('token', 'reset', {id: r.id}).then(function () {
            r.success = true
            return r
          })
        }
        else {
          return {statusCode: 404}
        }
      })
  },

  delete({id}) {
    return db.table('token')
      .where('id', id)
      .returning(['id', 'time', 'created'])
      .del()
      .then(([r]) => {
        if (r) {
          return this.log('token', 'delete', {id: r.id}).then(function () {
            r.success = true
            return r
          })
        }
        else {
          return {statusCode: 404}
        }
      })
  }
}
