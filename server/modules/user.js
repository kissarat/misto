const db = require('schema-db')
const bcrypt = require('promised-bcrypt')
const config = require('../../config')
const {pick} = require('lodash')
const request = require('request-promise')
const {createHash} = require('crypto')

function gavatar({email}) {
  const hash = createHash('md5')
  hash.update(email)
  const digest = hash.digest('hex')
  return request(`https://ru.gravatar.com/${digest}.json`, {
    headers: {
      accept: 'application/json',
      'user-agent': 'Sam'
    }
  })
    .then(function (gavatar) {
      console.log(gavatar)
      gavatar = JSON.parse(gavatar)
      return gavatar.entry[0]
    })
}

module.exports = {
  gavatar,

  login({nick, password}) {
    if (!nick || !password) {
      return {
        statusCode: 400,
        success: false
      }
    }
    return db.table('token_user')
      .where('nick', nick)
      .then(tokens => {
        if (tokens.length > 0) {
          const passwordToken = tokens[0]
          return bcrypt.compare(password, passwordToken.id)
            .then(success => {
              if (success) {
                return this.token.update({user: passwordToken.user.id}).then(function (token) {
                  token.success = true
                  token.user = passwordToken.user
                  return token
                })
              }
              return {
                success,
                status: 'ABSENT',
                error: {name: 'password'}
              }
            })
        }
        return {
          success: false,
          status: 'ABSENT',
          error: {name: 'nick'}
        }
      })
  },

  logout() {
    return this.token.update({user: null})
  },

  signup(data) {
    const required = {
      email: /^.*@.*\.\w+$/,
      nick: /^[\w_]+$/,
      password: /^.{6,128}$/
    }
    for (const name in required) {
      const value = data[name]
      if (!value || !required[name].test(value)) {
        return {
          statusCode: 400,
          status: 'INVALID_FIELD',
          error: {
            name,
            value
          }
        }
      }
    }
    return bcrypt.hash(data.password)
      .then((passwordTokenId) => {
        data = pick(data, 'nick', 'email')
        return db.table('user')
          .insert(data, ['id', 'nick', 'created', 'email'])
          .then(([user]) => {
            const data = {
              id: passwordTokenId,
              user: user.id,
              type: 'password',
              expires: new Date(2030, 1)
            }
            return db.table('token').insert(data)
              .then(() => {
                if (this.token) {
                  return this.token.update({user: user.id})
                }
              })
              .then(function () {
                return gavatar({email: user.email})
                  .then(function (gavatar) {
                    gavatar = {
                      gavatar: +gavatar.id,
                      avatar: gavatar.thumbnailUrl
                    }
                    return db.table('user')
                      .where('id', user.id)
                      .update(gavatar, ['gavatar', 'avatar'])
                  })
                  .catch(function (err) {
                    console.error(err)
                  })
              })
              .then(function (u) {
                user.success = true
                if (u && u.length > 0) {
                  user.gavatar = u.gavatar
                  user.avatar = u.avatar
                }
                return user
              })
          })
          .catch(function (err) {
            if (db.UNIQUE_VIOLATION === err.code) {
              return {
                statusCode: 400,
                status: 'INVALID_UNIQUE',
                error: {
                  name: err.constraint.replace('user_', '')
                }
              }
            }
            throw err
          })
      })
  },

  get(params) {
    return db.table('user').where(params).then(([user]) => user || {statusCode: 404})
  },

  me() {
    return this.user.guest ? {statusCode: 401} : this.user
  },


}
