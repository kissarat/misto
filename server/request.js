const db = require('schema-db')

module.exports = {
  log(entity, action, data, user, ip) {
    return db.log(entity, action, data, user || (this.user ? this.user.id : null), ip || this.headers.ip)
  }
}
